﻿using UnityEngine;
using System.Collections;

public class follow_object : MonoBehaviour {

    private Transform target;       // Reference to the player's transform.
    public string target_TAG;

    void Awake()
    {
        // Setting up the reference.
        target = GameObject.FindGameObjectWithTag("MainCamera").transform;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.position = new Vector3(target.transform.position.x, transform.position.y, transform.position.z);
    }
}
