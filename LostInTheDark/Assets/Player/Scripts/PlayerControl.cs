﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour {
    [HideInInspector]
    public bool facingRight = true;         // For determining which way the player is currently facing.
    [HideInInspector]
    public bool jump, walljump, airjump, inAir;    // to tell what jump action needs to happen
    [HideInInspector]
    public int airjumpLimit = 1;            // limit of in air jumps 

    public float moveForce = 8.0f; //8.0f;          // Amount of force added to move the player left and right.
    public float maxSpeed = 2.5f; //2.5f;           // The fastest the player can travel in the x axis.
    public float jumpForce = 4.5f; //4.5f           // Amount of force added when the player jumps.

    private SoundHandler SoundManager;
    private GameManager GameManager;

    private Transform groundCheckA, groundCheckB, wallCheckA, wallCheckB;          // A position marking where to check if the player is grounded.
    private Animator animator;                  // Reference to the player's animator component.
    private Rigidbody rb3d;                     // Reference to the player's rigidbody component.
    private Vector3 playerStart;                // to store players starting location

    private bool grounded, running, wallhug, jumpKeyToggle;      // Player is: grounded/running/facing wall/holding jump key.
    private bool backagainstwall;
    private float jumpCooldown, jumpTimer, h;      // to prevents jump bursts // h is horizontal input value
    private int airjumpCount;                   // max number of in air jumps before hit ground again

    //lightcontrol
    public float pickUpLightValue = 1f;                  // controls the amount of light added to the player when a lightpick happens
    public Light playerLight, spriteLight, sideLight;       // various player light components
    private int pickUpCounter, totalOrbsCounter;                  // counts light orbs player collects while player light change is in progress
                                                             // testCounter can be used to count something
    private ParticleSystemRenderer lightAura_R;
    private bool LightAddInProgress, LightDecayInProgress;  // if light change is in progress
    private float[] maxLightVarValues = new float[6];       // to stores values set in inspector as maxium
    private float[] targetLightVarValues = new float[6];    // to store target value for light add after orb pickups

    //GameStage // TEMP // WIP
    [HideInInspector]
    private int gameStage;              // acts as a gamestate control // 1 game IP // 2 gameover
    public GameObject GameOver, ExitObject;         // shows something for the player if gameStage 2

    // Store a targetLocation as a Vector xyz
    private Vector3 posTarget;

    // GameObject Target
    private GameObject targetObject;

    //Debug Log Message
    private string DLM, tDLM;

    // property to get gameStage
    public int GameStage
    {
        get
        {
            return gameStage;
        }
    }

    void Start()
    {
        gameStage = 1;
        lightAura_R = transform.Find("FX/LightAura").GetComponent<ParticleSystemRenderer>();

        //PlayerLight: range, inte; LightAura: minPS, maxPS; SideLight: inte; SpriteLight: inte
        maxLightVarValues = new float[6] {
            playerLight.range, playerLight.intensity,
            lightAura_R.minParticleSize, lightAura_R.maxParticleSize,
            sideLight.intensity,
            spriteLight.intensity
        };

        // Debug related
        DLM = "";
        totalOrbsCounter = 0;

        // jump related
        jumpCooldown = 0.12f; // dTime is set at 0.02 so roughly 1.2 sec for 60 frames
        jumpTimer = 0;
        grounded = false;
        running = false;
        inAir = false;

        // light related
        pickUpCounter = 0;
        LightAddInProgress = false;

        // Begin light decay on player @start
        if (gameStage == 1) {
            StartCoroutine(PlayerLightDecay());
            LightDecayInProgress = true;
        }
        else {
            LightDecayInProgress = false;
        }
    }

    void Awake()
    {
        // Get references to the manager object scripts
        GameManager = FindObjectOfType<GameManager>();
        SoundManager = FindObjectOfType<SoundHandler>();
        
        // Setting up some player child object references.
        playerStart = transform.position;
        //Debug.Log("Player@" + playerStart.ToString());
        rb3d = GetComponent<Rigidbody>();
        groundCheckA = transform.Find("GroundCheckA");
        groundCheckB = transform.Find("GroundCheckB");
        wallCheckA = transform.Find("WallCheckA");
        wallCheckB = transform.Find("WallCheckB");
        animator = GetComponent<Animator>();
        //playerLight = transform.Find("FX").FindChild("PlayerLight").GetComponent<Light>();
    }


    void Update()
    {
        // The player is grounded if a linecast to the groundcheck position hits anything on the ground layer.
        grounded = Physics.Linecast(transform.position, groundCheckA.position, 1 << LayerMask.NameToLayer("Ground")) ||
            Physics.Linecast(transform.position, groundCheckB.position, 1 << LayerMask.NameToLayer("Ground"));
        // Is player hugging a wall?
        wallhug = Physics.Linecast(transform.position, wallCheckA.position, 1 << LayerMask.NameToLayer("Ground"));
        backagainstwall = Physics.Linecast(transform.position, wallCheckB.position, 1 << LayerMask.NameToLayer("Ground"));

        JumpFunction("getInput");

    }


    void FixedUpdate()
    {

        // Get horizontal input.
        h = Input.GetAxis("Horizontal");
        //Debug.Log(h);

        //CheckGameStageChangesTriggeredByPlayer
        CheckIfEnded();

        // Run animation control
        if (grounded) {
            // check if landed and play a sound
            if (inAir) {
                SoundManager.soundLand(PreferenceManager.SfxVolume);
                inAir = false;
                animator.SetBool("Jump", false);
            }
            else if (!jump && Mathf.Abs(rb3d.velocity.y) < 0.01f) {
                animator.SetBool("Jump", false);
            }

            if (Mathf.Abs(rb3d.velocity.x) < 0.01f && Mathf.Abs(rb3d.velocity.y) < 0.01f) {
                animator.SetBool("Run", false);
            }
            else if (Mathf.Abs(h) > 0) {
                animator.SetBool("Run", true);
                if(!SoundManager.step.isPlaying) {
                    SoundManager.soundStep(PreferenceManager.SfxVolume);
                }
            }
            else if (h == 0 && Mathf.Abs(rb3d.velocity.y) < 0.01f) {
                animator.SetBool("Run", false);
                rb3d.velocity = new Vector3(0, 0, 0);
            }
        }
        /*
        else if (wallhug && Mathf.Abs(rb3d.velocity.y) < 0.005f) {
            // if option block
                //Mathf.Abs(rb3d.velocity.x) < 0.01f && Mathf.Abs(rb3d.velocity.y) < 0.01f
                //jumpTimer <= jumpCooldown ||
            
            // ignore horizontal input while jump cooldown is active // or if stuck
            h = 0;
        }
        */


        // If the player is changing direction (h has a different sign to velocity.x) or hasn't reached maxSpeed yet...
        if (h * rb3d.velocity.x < maxSpeed)
            // ... add a force to the player.
            rb3d.AddForce(Vector3.right * h * moveForce, ForceMode.Acceleration);

        // If the player's horizontal velocity is greater than the maxSpeed...
        if (Mathf.Abs(rb3d.velocity.x) > maxSpeed)
            // ... set the player's velocity to the maxSpeed in the x axis.
            rb3d.velocity = new Vector3(Mathf.Sign(rb3d.velocity.x) * maxSpeed, rb3d.velocity.y);

        // If the input is moving the player right and the player is facing left...
        if (h > 0 && !facingRight) {
            // ... flip the player.
            Flip();
        }

        // Otherwise if the input is moving the player left and the player is facing right...
        else if (h < 0 && facingRight) {
            // ... flip the player.
            Flip();
        }

        // Player Fall animation stage control
        if (!jump && !airjump && !walljump && rb3d.velocity.y < -0.2f) {
            animator.SetBool("Falling", true);
            animator.SetBool("Run", false);
            animator.SetBool("Jump", false);
            // if falling and not hit ground
            if (!grounded) {
                inAir = true;
            }
        }
        else if (grounded && rb3d.velocity.y >= 0) {
            animator.SetBool("Falling", false);
        }

        // apply forces to player to do various jumps
        JumpFunction("doJump");


    } //end of fixed update

    void LateUpdate()
    {
        // if player picks up something
        LightCheck();

        // Debug Message compiler
        //PrintDebugMessage();
    }

    public void PrintDebugMessage()
    {
        // DEBUG LOG SECTION for Fixed Update
        tDLM = "";
        //tDLM = "X-movevement: " + rb3d.velocity.x + "; Y-movement: " + rb3d.velocity.y +"; ";
        //tDLM += "Grounded: " + grounded + "; ";
        //tDLM += "WallCheck: " + wallhug + "; ";
        tDLM += "tCounter: " + totalOrbsCounter + "; ";
        tDLM += "puCounter: " + pickUpCounter + "; ";
        //tDLM += "Decay: " + LightDecayInProgress + "; ";
        tDLM += "LightAdd: " + LightAddInProgress + "; ";
        //tDLM += "Jumps(J/DJ/WJ):" + jump + " " + airjump + " " + walljump + "; ";
        //tDLM += "jumpTimer:" + jumpTimer + "; ";
        //tDLM += "dTime: " + Time.deltaTime + "; ";

        if (!string.Equals(tDLM, DLM)) {
            /*
            DLM = "";
            foreach (var item in maxLightVarValues)
            {
                DLM = DLM + " " + item;
            }
            Debug.Log(DLM);
            */
            DLM = tDLM;
            Debug.Log(DLM);
        }
    }


    void Flip()
    {
        // Switch the way the player is labelled as facing.
        facingRight = !facingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    void OnTriggerEnter(Collider other)
    {
        //Check the provided Collider parameter other to see if it is tagged "LightOrb", if it is...
        if (other.gameObject.CompareTag("LightOrb")) {
            // send the other object reference to the method

            //Debug.Log(other.gameObject.name);
            StopCoroutine("OrbHideDelay");
            StartCoroutine(OrbHideDelay(other));
        }
        // If player drops off the level this ports the character back to start
        if (other.gameObject.CompareTag("PlayerCatcher")) {
            //Debug.Log("PlayerCaught");
            SoundManager.soundRip(PreferenceManager.SfxVolume);
            rb3d.transform.position = playerStart;
        }
        // player collides with exit sets game stage to win state
        if (other.gameObject.CompareTag("Exit")) {
            Debug.Log("ExitFound");
            targetObject = other.gameObject.gameObject;
            posTarget = new Vector3(other.transform.position.x, other.transform.position.y, transform.position.z);
            rb3d.velocity = new Vector3(0, 0, 0);
            rb3d.useGravity = false;
            gameStage = 3;
        }
    }

    // LightOrb Collection and Hiding
    IEnumerator OrbHideDelay(Collider orb)
    {
        yield return new WaitForSeconds(0.05f);
        if (orb.gameObject.activeInHierarchy) {
            pickUpCounter++;
            totalOrbsCounter++;
            SoundManager.soundPickup(PreferenceManager.SfxVolume);

            //Debug.Log("OrbCollected! " + totalOrbsCounter); // there are testCounters for debug commented out elsewhere
        }
        orb.gameObject.SetActive(false);

    }

    //gradually changes playerlight
    IEnumerator AddPlayerLight()
    {
        yield return new WaitUntil(() => !LightDecayInProgress);
        Debug.Log("Check_1: OK");

        // divider row: 1, 4, 4, 10, 10, 1, 4
        targetLightVarValues = new float[6] {
            playerLight.range + (pickUpLightValue / 1f),
            playerLight.intensity + (pickUpLightValue / 4f),
            lightAura_R.minParticleSize + (pickUpLightValue / 4f),
            lightAura_R.maxParticleSize + (pickUpLightValue / 4f),
            sideLight.intensity + (pickUpLightValue / 1f),
            spriteLight.intensity + (pickUpLightValue / 4f)               //NEED TO ADJUST THESE VALUES
        };
        for (int i = 0; i < maxLightVarValues.Length; i++) {
            //Debug.Log(targetLightVarValues[i] + "==" + maxLightVarValues[i]);
            if (targetLightVarValues[i] > maxLightVarValues[i]) {
                targetLightVarValues[i] = maxLightVarValues[i];
                //Debug.Log(targetLightVarValues[i] + "==" + maxLightVarValues[i]);
            }
        }

        // if currentvalue + usedvalue < target && target <= maxvalue
        int targetsMet = 0;
        while (targetsMet < 6) {

            float speed = Time.deltaTime * 20f;
            //yield return new WaitForSeconds(Time.deltaTime);
            yield return null;


            // addrow: 0.04, 0.01, 0.004, 0.004, 0,04, 0.01
            playerLight.range = playerLight.range + 0.04f * speed;
            playerLight.intensity = playerLight.intensity + 0.01f * speed; //0.01f
            lightAura_R.minParticleSize = lightAura_R.minParticleSize + 0.010f * speed; //0.004f rate gets to 0 at range 0
            lightAura_R.maxParticleSize = lightAura_R.maxParticleSize + 0.010f * speed;
            sideLight.intensity = sideLight.intensity + 0.04f * speed;//0.04f
            spriteLight.intensity = spriteLight.intensity + 0.01f * speed;

            // Check all 6 variables
            targetsMet = 0;

            if (playerLight.range >= targetLightVarValues[0]) {
                playerLight.range = targetLightVarValues[0];
                targetsMet++;
            }
            if (playerLight.intensity >= targetLightVarValues[1]) {
                playerLight.intensity = targetLightVarValues[1];
                targetsMet++;
            }
            if (lightAura_R.minParticleSize >= targetLightVarValues[2]) {
                lightAura_R.minParticleSize = targetLightVarValues[2];
                targetsMet++;
            }
            if (lightAura_R.maxParticleSize >= targetLightVarValues[3]) {
                lightAura_R.maxParticleSize = targetLightVarValues[3];
                targetsMet++;
            }
            if (sideLight.intensity >= targetLightVarValues[4]) {
                sideLight.intensity = targetLightVarValues[4];
                targetsMet++;
            }
            if (spriteLight.intensity >= targetLightVarValues[5]) {
                spriteLight.intensity = targetLightVarValues[5];
                targetsMet++;
            }
        }
        //yield return new WaitForEndOfFrame();
        pickUpCounter--;
        Debug.Log("LightAddEnd pUc: " + pickUpCounter);
        LightAddInProgress = false;

    }

    // over time Light Decay
    IEnumerator PlayerLightDecay()
    {
        Debug.Log("Begin Light Decay!");
        while (gameStage == 1 || gameStage == 2) {
            // Time it took to render last frame 
            // if 60 frames per sec then have to multiply these values with it
            float speed = Time.deltaTime * 2.5f;
            // set fixed delay
            //speed = 0.016f * 2.5f;
            //yield return null;
            yield return new WaitForSeconds(0.016f);

            if (!LightAddInProgress && playerLight.range > 0.8) {  // 
                //subtractrow: 0.04, 0.01, 0.004, 0.004, 0,04, 0.01
                playerLight.range = playerLight.range - 0.04f * speed;
                playerLight.intensity = playerLight.intensity - 0.01f * speed; //0.01f
                lightAura_R.minParticleSize = lightAura_R.minParticleSize - 0.010f * speed; //0.004f rate gets to 0 at range 0
                lightAura_R.maxParticleSize = lightAura_R.maxParticleSize - 0.010f * speed; //0.008 works for 0.8 startsize
                sideLight.intensity = sideLight.intensity - 0.04f * speed;//0.04f
                spriteLight.intensity = spriteLight.intensity - 0.01f * speed;

            }
            else if (playerLight.range <= 0.9) {
                gameStage = 2;
            }
            else {
                LightDecayInProgress = false;
                Debug.Log("Decay on hold.");
                yield return new WaitUntil(() => pickUpCounter == 0);
                LightDecayInProgress = true;
            }
        }
        //UnityEngine.SceneManagement.SceneManager.LoadScene(3);

    }

    void LightCheck()
    {
        if (!LightAddInProgress && pickUpCounter > 0) {
            //StopCoroutine("PlayerLightDecay");
            Debug.Log("LAIP");
            LightAddInProgress = true;
            StartCoroutine(AddPlayerLight());

        }
    }


    // Function to handle double jump related things takes in a boolean to separate update and fixed update
    void JumpFunction(string jState)
    {
        switch (jState) {
            // checks for jump input // @Update
            case "getInput":

                // is the jump button pressed but not already down
                if (!jumpKeyToggle && Input.GetButtonDown("Jump") && jumpTimer >= jumpCooldown) {

                    // start jump cooldown and enable key antirepeat
                    jumpTimer = 0;
                    jumpKeyToggle = true;

                    // player standing on ground
                    if (grounded && !jump) {
                        jump = true;
                    }

                    // walljump using raycast behind char result
                    else if (backagainstwall && !walljump) {
                        walljump = true;
                    }

                    // in air not done an air jump yet
                    else if (airjumpCount < airjumpLimit && !airjump) {
                        airjump = true;
                        airjumpCount++;
                    }
                    if (jump || walljump || airjump) {
                        if(!walljump) {
                            animator.SetBool("Falling", false);
                            animator.SetBool("Run", false);
                            animator.SetBool("Jump", true);
                        }
                        else {
                            animator.SetBool("Falling", false);
                            animator.SetBool("Jump", false);
                            animator.SetBool("Run", true);
                        }
                        // Call jump SFX
                        if (gameStage == 1) {
                            SoundManager.soundJump(PreferenceManager.SfxVolume);
                        }
                        //Debug.Log(GameManager.sfxVol + "\t" + GameManager.masterVol);
                    }
                }
                // check for key release
                if (Input.GetButtonUp("Jump") && jumpKeyToggle) {
                    jumpKeyToggle = false;
                }
                // check jump key cooldown, and move towards if not equal
                if (jumpTimer < jumpCooldown) {
                    jumpTimer += Time.deltaTime;
                }
                break;

            // applies force to player // @FixedUpate
            case "doJump":
                // If the player should jump...
                if ((jump || airjump) && !walljump) {
                    
                    
                    //Debug.Log("A Jump Dedected!");
                    

                    if (jump) {
                        // Add a vertical force to the player.
                        rb3d.AddForce(new Vector3(0f, jumpForce), ForceMode.Impulse);
                        jump = false;
                    }
                    else {
                        rb3d.velocity = new Vector3(rb3d.velocity.x, 0.015f, 0);
                        rb3d.AddForce(new Vector3(0f, jumpForce * 1.1f), ForceMode.Impulse);
                        airjump = false;
                    }
                }
                // if walljump
                else if (walljump) //&& Mathf.Abs(rb3d.velocity.x) > 0.5f)
                {
                    float addForce = (!facingRight) ? (-1 * maxSpeed) : maxSpeed;
                    addForce *= 2.0f;
                    rb3d.velocity = new Vector3(0, 0.5f, 0);
                    rb3d.AddForce(new Vector3(addForce, jumpForce), ForceMode.Impulse);
                    walljump = false;
                }
                else {
                    if (grounded) {
                        airjumpCount = 0;
                    }
                }
                break;

            default:
                break;
        }

    }

    // makes sure player is more committed // gives horizontal input away from the wall after jump key
    IEnumerator WalljumpSafetyCheck(bool facedR)
    {
        //yield return new WaitUntil(() => !jumpKeyToggle);
        yield return new WaitUntil(() => !jumpKeyToggle);    //Input.GetButtonUp("Jump"));
        //yield return new WaitForFixedUpdate();
        //yield return new WaitForEndOfFrame();
        if (facedR != facingRight) {
            if (!grounded && Physics.Linecast(transform.position, wallCheckB.position, 1 << LayerMask.NameToLayer("Ground"))) {
                walljump = true;
            }
        }
        else {
            if (airjumpCount < airjumpLimit) {
                airjump = true;
                airjumpCount++;
            }
        }
    }

    // Do stuff based on which gameStage is going on while a level scene is active
    public void CheckIfEnded()
    {
        if (gameStage > 1) {
            h = 0;
            jump = false;
            airjump = false;
            walljump = false;
            if (grounded && gameStage == 2) {
                animator.SetBool("Sleep", true);
                GameOver.gameObject.SetActive(true);
            }
            else if (gameStage == 3) {
                // do finishing animations or show something on GUI

                // holds player in place after colliding with the exit  
                /* moved these lines to the end of gameStage 2
                    rb3d.velocity = new Vector3(0,0,0);
                    rb3d.useGravity = false;
                */

                // Activate Exit text GUI
                if (!ExitObject.gameObject.activeInHierarchy) {
                    ExitObject.gameObject.SetActive(true);
                }

                // moves Player TowardsExit
                if (transform.position.x != posTarget.x || transform.position.y != posTarget.y) {
                    Vector3 thePos = transform.position;

                    if (transform.position.x != posTarget.x) {
                        thePos.x = Mathf.MoveTowards(thePos.x, posTarget.x, 0.008f);
                    }
                    if (transform.position.y != posTarget.y) {
                        thePos.y = Mathf.MoveTowards(thePos.y, posTarget.y, 0.008f);
                    }
                    transform.position = thePos;
                }

                // shirnks player down frame by frame after finding the exit to create a vanish visual effect
                if (transform.lossyScale.y > 0) {
                    Vector3 theScale = transform.lossyScale;
                    
                    if(facingRight) {
                        // scale x is positive 
                        theScale.x -= 0.015f;
                    } else {
                        // scale x is negative 
                        theScale.x += 0.015f;
                    }
                    theScale.y -= 0.015f;
                    transform.localScale = theScale;
                    
                } else if (targetObject.transform.lossyScale.y > 0.0001f) {
                    Vector3 ScaleMulti = new Vector3(targetObject.transform.lossyScale.x * 0.975f, targetObject.transform.lossyScale.y * 0.975f, targetObject.transform.lossyScale.z * 0.975f);
                    targetObject.transform.localScale = ScaleMulti;
                    playerLight.range = playerLight.range * 0.975f;
                    playerLight.intensity = playerLight.intensity * 0.975f;
                    lightAura_R.minParticleSize = lightAura_R.minParticleSize * 0.975f;
                    lightAura_R.maxParticleSize = lightAura_R.maxParticleSize * 0.975f;
                    targetObject.GetComponent<ParticleSystemRenderer>().maxParticleSize *= 0.975f;
                    targetObject.GetComponent<ParticleSystemRenderer>().minParticleSize *= 0.975f;
                    targetObject.GetComponent<Light>().range *= 0.975f;
                    targetObject.GetComponent<Light>().intensity *= 0.975f;
                } else {
                    targetObject.SetActive(false);
                    transform.gameObject.SetActive(false);
                }
            }
        }
    }
}
