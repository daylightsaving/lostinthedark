﻿using UnityEngine;
using System.Collections;

public class SoundHandler : Singleton<SoundHandler> {
	
	public AudioSource bgMusic;
	public AudioSource click;
	public AudioSource jump;
	public AudioSource land;
	public AudioSource gameOver;
    public AudioSource step;
    public AudioSource pickup;
    public AudioSource rip;
	
	public void soundClick(float volume){
		playSound(click,volume);
	}
	
	public void soundJump(float volume){
        playSound(jump,volume);
	}
	
	public void soundLand(float volume){
		playSound(land,volume);
	}
	public void soundGameOver(float volume){
		playSound(gameOver,volume);
	}
	public void soundBg(float volume){
		playSound(bgMusic,volume);
	}
	public void soundStep(float volume){
        playSound(step,volume);
    }
    public void soundPickup(float volume)    {
        playSound(pickup,volume);
    }
    public void soundRip(float volume){
        playSound(rip, volume);
    }
	public void pauseBg(){
		bgMusic.Pause();
	}
	
	void playSound(AudioSource sound,float currentVolume){
		if(sound != null) {
			sound.volume = PreferenceManager.MasterVolume * currentVolume;
			sound.Play();
		}
	}
	
	void stopSound(AudioSource sound){
		if(sound != null) {
			sound.Stop();
		}
	}

    
}
