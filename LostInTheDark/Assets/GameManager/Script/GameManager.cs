﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; // for UI elements
using UnityEngine.SceneManagement; // scene Manager

public class GameManager : Singleton<GameManager> {

    private static GameManager _instance;
    public static GameManager instance { get { return _instance; } }              //Static instance of GameManager which allows it to be accessed by any other script.

    private enum GameState {
        InitGame = 0,
        MainMenu = 1,
        Loading = 2,
        Gameplay = 3,
        Scores = 4
    }

    // Gamestate Control
    private int gameState;
    private int loadingScene, restart;
    private bool loading;
    public float loadingDelay;

    // Settings
    private float brightness;
    [HideInInspector]
    public float masterVol, musicVol, sfxVol;

    // SoundManager ref
    private SoundHandler SoundManager;

    // Brightness Adjustment Light
    private Light Brightness_DirLight;

    // Player
    private GameObject Player;
    private PlayerControl PlayerCtrl;

    // UI Control
    public GameObject pMenuPanel, goMenuPanel; // pause and gameover references
    private bool showPaused; 
    private float timeScale; // store default timescale


    private void Awake()
    {
        //Check if instance already exists
        if (_instance != null && _instance != this) {
            Destroy(gameObject);
        } else {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }

    // Use this for initialization
    void Start () {
        timeScale = Time.timeScale;
        Brightness_DirLight = this.GetComponentInChildren<Light>();
        Brightness_DirLight.intensity = 0.05f;
        SoundManager = FindObjectOfType<SoundHandler>();

        masterVol = PreferenceManager.MasterVolume;
        musicVol = PreferenceManager.MusicVolume;
        sfxVol = PreferenceManager.SfxVolume;
        

        // no matter where if there is a GameManager on the scene the game starts from the MainMenu
        Exit_To_Main();
    }
	
	// Update is called once per frame
	void Update () {
        switch (gameState) {
            case 0:
                if (SceneManager.GetActiveScene().buildIndex == (int)GameState.MainMenu) {
                    gameState = (int)GameState.MainMenu;
                    // not used atm but the scene has a GameManager gameobject
                }
                break;
            case 1:
                if (restart != 0) {
                    Begin_Loading_OnClick(restart);
                    restart = 0;
                    if (SceneManager.GetActiveScene().buildIndex >= (int)GameState.MainMenu) {
                        SoundManager = FindObjectOfType<SoundHandler>();

                        if (SoundManager != null && SoundManager.bgMusic.isPlaying == false) {
                            SoundManager.soundBg(PreferenceManager.MusicVolume);
                        }
                        else if (SoundManager.bgMusic.volume != PreferenceManager.MusicVolume) {
                            SoundManager.bgMusic.volume = PreferenceManager.MusicVolume;
                        }
                    }
                }
                break;
            case 2:
                if (!loading && SceneManager.GetActiveScene().buildIndex == (int)GameState.Loading) {
                    loading = true;
                    Invoke("Load_Game_Scene", loadingDelay);
                } else if (SceneManager.GetActiveScene().buildIndex == (int)GameState.Gameplay) {
                    loading = false;
                    gameState = (int)GameState.Gameplay;
                    PlayerCtrl = FindObjectOfType<PlayerControl>();
                }
                break;
            case 3:
                if (PlayerCtrl.GameStage == 1) {
                    // Game On
                }
                else if (PlayerCtrl.GameStage == 2) {
                    // Game Over
                }
                else if (PlayerCtrl.GameStage == 3) {
                    // Level Finished
                    // could move set stage to Scores after a delay and then load scores scene
                }
                if (Input.GetButtonDown("Cancel")) {
                    Pause_Control();
                }
                break;
        }
        // to keep Music Volume under control
        if (SoundManager.bgMusic.volume != PreferenceManager.MusicVolume * PreferenceManager.MasterVolume) {
            SoundManager.bgMusic.volume = PreferenceManager.MusicVolume * PreferenceManager.MasterVolume;
        }
    }

    // Get the Build Index of the targeted LevelScene, but do a LoadingScene in between
    public void Begin_Loading_OnClick(int sceneIndex)
    {
        loadingScene = sceneIndex;
        gameState = (int)GameState.Loading;
        SceneManager.LoadScene((int)GameState.Loading);
    }
    // Is called after the delay on the LoadingScreen Scene
    public void Load_Game_Scene ()
    {
        SceneManager.LoadScene(loadingScene);
    }
    // Sets a level to be restarted
    public void Restart_Level ()
    {
        restart = SceneManager.GetActiveScene().buildIndex;
        Exit_To_Main();
    }
    // Go to Main Menu
    public void Exit_To_Main ()
    {
        Check_Pause();
        gameState = (int)GameState.MainMenu;
        SceneManager.LoadScene((int)GameState.MainMenu);
    }
    // while in gameplay-state: Esc button or Click resume
    public void Pause_Control()
    {
        if (gameState == 3) {

            if (PlayerCtrl.GameStage == 1) {

                if (showPaused) {
                    foreach (Transform item in pMenuPanel.transform) {
                        if (item.name == "MenuPanel") {
                            item.gameObject.SetActive(true);
                        }
                        else if (item.name == "SettingsPanel") {
                            item.gameObject.SetActive(false);
                        }
                    }
                    Time.timeScale = timeScale;
                    pMenuPanel.SetActive(false);

                    showPaused = false;
                }
                else if (!showPaused) {
                    Time.timeScale = 0.0f;
                    pMenuPanel.SetActive(true);

                    showPaused = true;
                }
            }
            else if (PlayerCtrl.GameStage > 1) {

                if (showPaused) {

                    goMenuPanel.SetActive(false);
                    showPaused = false;
                }
                else if (!showPaused) {

                    goMenuPanel.SetActive(true);
                    showPaused = true;
                }
            }
        }
    }
    // Check if pause is on, and unpause it
    private void Check_Pause()
    {
        Time.timeScale = (Time.timeScale != 1.0f) ? 1.0f : Time.timeScale;
        pMenuPanel.SetActive(false);
        goMenuPanel.SetActive(false);
        if (showPaused) showPaused = false;
    }
    public void Adjust_Brightness ()
    {
        // Need to make sure Unity uses this path to the object or this will break!
        brightness = GameObject.Find("/GameManager/UI/PauseMenuCanvas/SettingsPanel/BrightnessSlider").GetComponent<Slider>().value;
        // Prints the current value next to the setting label
        GameObject.Find("/GameManager/UI/PauseMenuCanvas/SettingsPanel/BrightnessLabel").GetComponent<Text>().text = "Brightness (" + (brightness) + ")";
        //Debug.Log("SliderInput: " + brightness);

        // Controls DirectLight on Everything within parameters of 0 ~ 0.04 (default 0.02)
        Brightness_DirLight.intensity = 0.05f + (brightness * 0.001f);
    }
    public void Adjust_Master_Volume ()
    {
        // handled by SettingsSlider & PreferenceManager
        
        // Need to make sure Unity uses this path to the object or this will break!
        //masterVol = GameObject.Find("/GameManager/UI/PauseMenuCanvas/SettingsPanel/MasterVolSlider").GetComponent<Slider>().value / 1;
        // Prints the current value next to the setting label // atm no text field
        //GameObject.Find("/GameManager/UI/PauseMenuCanvas/SettingsPanel/MasterVolLabel").GetComponent<Text>().text = "Master Volume (" + (masterVol) + ")";
        //PreferenceManager.MasterVolume = masterVol;

    }
    public void Adjust_Music_Volume()
    {
        // handled by SettingsSlider & PreferenceManager

        // Need to make sure Unity uses this path to the object or this will break!
        //musicVol = GameObject.Find("/GameManager/UI/PauseMenuCanvas/SettingsPanel/MusicVolSlider").GetComponent<Slider>().value / 1;
        // Prints the current value next to the setting label // atm no text field
        //GameObject.Find("/GameManager/UI/PauseMenuCanvas/SettingsPanel/MusicVolLabel").GetComponent<Text>().text = "Music Volume (" + (musicVol) + ")";
        //PreferenceManager.MusicVolume = musicVol;

    }
    public void Adjust_SFX_Volume()
    {
        // handled by SettingsSlider & PreferenceManager

        // Need to make sure Unity uses this path to the object or this will break!
        //sfxVol = GameObject.Find("/GameManager/UI/PauseMenuCanvas/SettingsPanel/SFXVolSlider").GetComponent<Slider>().value / 1;
        // Prints the current value next to the setting label // atm no text field
        //GameObject.Find("/GameManager/UI/PauseMenuCanvas/SettingsPanel/SFXVolLabel").GetComponent<Text>().text = "SFX Volume (" + (sfxVol) + ")";
        //PreferenceManager.SfxVolume = sfxVol;

    }
}
