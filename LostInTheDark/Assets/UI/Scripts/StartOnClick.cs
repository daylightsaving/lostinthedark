﻿using UnityEngine;
using System.Collections;

public class StartOnClick : MonoBehaviour {

    
    // the uglier way to load first level but might offer more ways of use
    public void DoWhat(string action)
    {
        GameManager gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        switch (action) {
            case "start":
                // begin level 1
                gm.Begin_Loading_OnClick(3);
                break;
            case "gotomain":
                // return to main menu
                gm.Exit_To_Main();
                break;
        }
    }

	public void OnSoundChanged(float val)
	{
		PreferenceManager.MasterVolume = val;
	}
    // the other way to ask gamemanager to start loading desired scene
    /*
    public void OnClick (int sceneID) {
        GameObject.Find("GameManager").GetComponent<GameManager>().Begin_Loading_OnClick(sceneID);
    }
    */
}