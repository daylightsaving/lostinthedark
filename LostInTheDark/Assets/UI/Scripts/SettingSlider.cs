﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SettingSlider : MonoBehaviour {

	public Slider masterSlider;
	public Slider musicSlider;
	public Slider sfxSlider;

	public void Start()
	{
        masterSlider.value = PreferenceManager.MasterVolume * 100;
        musicSlider.value = PreferenceManager.MusicVolume * 100;
        sfxSlider.value = PreferenceManager.SfxVolume * 100;
        masterSlider.onValueChanged.AddListener (delegate {MasterSliderChanged ();});
		musicSlider.onValueChanged.AddListener (delegate {MusicSliderChanged ();});
		sfxSlider.onValueChanged.AddListener (delegate {SfxSliderChanged ();});
	}

	public void MasterSliderChanged()
	{
		PreferenceManager.MasterVolume = masterSlider.value;
	}
	public void MusicSliderChanged()
	{
		PreferenceManager.MusicVolume = musicSlider.value;
	}
	public void SfxSliderChanged()
	{
		PreferenceManager.SfxVolume = sfxSlider.value;
	}

}
