### **Week 48** ####

- DevBuild
	- Music and SFX added. BackGroundMusic Credit: https://www.youtube.com/watch?v=wWjgsepyE8I
	- SoundHandler and sound related setting scripts added
	- Small TestScene edits and Occulsion Culling enabled with the MainCamera

- WIP
	- 3D-playerCharacter and level expansion

- ToDo  
	- SplashScreen and icon suitable for the game.

### **Week 46** ####

- DevBuild 
	- added level Exit-object, and a transition effect for it
	- a background sky added, and a script for its position control
	- player light effect(s) adjusted towards a bit more visible
	- walljump triggerpoint adjusted to back against wall and jump input

-  Work in progress
	- a 3D-version of the player character
	- an overlay for the camera

- ToDo
	- Level building/decorating
	- Music/soundFX

### **Week 45** ####

- Meetings 
	- Due to the Capstone Project work schedule our weekly meeting spot has now been moved to the late Tuesday afternoons 14-16 spot

- Game related
	- Thanks to Rauli we now have an enhanced look to our GUI elements!
	- The game now has GameManager implemented which will act as a game control object that wont get destroyed when scenes change
		- also lessens the need for multiple small script files here and there
		- handles the game settings, states and scene loads

- ToDo
	- Our level could perhaps use some decorating
	- Music System?

### **Week 44** ####

- ToDo: 
	- Plans for the new meeting schedule (when we know more)
	- Prototype presentation on Thursday 
		- via USB (need to store a working build for that)
- Pause menu has been added (P-button)
- Nature assets have been added to the assets

### **Week 43** ####

- Test Scene 
	- Level has been extended
- Player mechanics
	- walljump 
		1. move towards a wall while in air
		2. once touching the wall start holding jump
		3. move away from the wall while holding jump
		4. when player turns away from the wall release the jump button fast
	- in air jump 
		- works only once after a jump then need to revisit ground
- Interface
	- a loading scene has been added in between main menu and the test scene

### **Week 42** ####

- AutumBreak

- Added light decay and pickup effects for different light sources around player
- Edited animation state code, and also added sleep frames for game over

### **Week 41** ####

- made a new repo for the project where only 2 folders under the projectfolder are shared and the rest will be ignored or added to the gitignore if needed
	- if the project is opened for the first time in Unity it creates an empty scene but our scenes are inside the scenes folder
- new version has both the MainMenu and the TestScene
	- if you want to work on a level or something then duplicate the TestScene and begin working from there, or even create a completely new one

### **Week 40** ###

**Progress/tuesday:**

- made sure everyone is doing something
- added a few new features (interface/menu, rough light mechanics after an orb pickup)


### **Week 39** ###

**ConceptDoc:**

- Tuesday's document version has been put into the return box

**The New Version (cleanmaster):**

- Contains a bit more organized project folder structure 
	- templates for some background level 3D-objects
	- scene to expand on
	- prefabs for GameObjects used on the scene
	- StoreAssets section

- Player Character
	- added scripted halt if no X-axis input && not enough Y-axis velocity (as requested)
		- will not work nicely with traps or anything that needs to push the player using added force
		- likely to be refined / worked around very soon
	- double jump added
		- simple 
		- no different animation for now
		- refinable and also expandable for further iterations (triple jumps etc)

### **Week 38** ###

**Mechanics:**

- Set Fixed Camera distance + Camera
- Basic character controls
- Sprite Animations
- Test environment scene build
- Temp background

**Others:**

- Check progress of each member
- Update Trello
- Upload early version with above ^ mechanics
- Make sure everyone gets access to the latest build
- Decide the game name?
- Update plans

### **Week 37** ###

**Project Starts!**


**Tasks done / decided:**

- Game Concept
- Roles decided
- Tasks handled
- Character concept
- Unity learning
- Register Bitbucket
- Create Facebook group
- Starting to implement basic features


This post will be kept updated for now.