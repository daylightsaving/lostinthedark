﻿using UnityEngine;
using System.Collections;

public class PreferenceManager {

	public const string Key_MasterVolume = "MasterVolume";
	public static float MasterVolume
	{
		get{ 
			return PlayerPrefs.GetFloat (Key_MasterVolume, 1f) / 100f;
		}
		set{ 
			PlayerPrefs.SetFloat (Key_MasterVolume, value);
		}
	}

	public const string Key_MusicVolume = "MusicVolume";
	public static float MusicVolume
	{
		get{ 
			return PlayerPrefs.GetFloat (Key_MusicVolume, 1f) / 100f;
		}
		set{ 
			PlayerPrefs.SetFloat (Key_MusicVolume, value);
		}
	}

	public const string Key_SfxVolume = "SfxVolume";
	public static float SfxVolume
	{
		get{ 
			return PlayerPrefs.GetFloat (Key_SfxVolume, 1f) / 100f;
		}
		set{ 
			PlayerPrefs.SetFloat (Key_SfxVolume, value);
		}
	}
}
